#!/bin/bash

# This is a build script to run make for all the supported GLUON_TARGETS and our gluon site configurations.
# Copyright (C) 2017,2019 Vieno Hakkerinen
# SPDX-License-Identifier: MIT

. "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/env.sh"

GLUON_BRANCH="nightly"

export GLUON_BRANCH

for TARGET in $ALL_TARGETS; do
	echo "### starting to build $TARGET ###"
        if ! make -j"$(nproc || echo -n 1)" GLUON_TARGET="$TARGET" BROKEN=1; then
		exit
	fi
	echo "### finished to build $TARGET ###"
done
