# ffc-build-script  

## description
This is a build script to run make for all the supported GLUON_TARGETS and our gluon site configurations.

## used external repositories
You can find the gluon repository at [github.com/freifunk-gluon/gluon/](https://github.com/freifunk-gluon/gluon/).

You can find the [Freifunk Chemnitz](https://www.chemnitz.freifunk.net/) site configurations at [gitlab.com/FreifunkChemnitz/site-ffc](https://gitlab.com/FreifunkChemnitz/site-ffc).

## license
This is a build script to run make for all supported GLUON_TARGETS and our gluon site configurations.

Copyright (C) 2017,2019,2020 Vieno Hakkerinen

Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice (including the next paragraph) shall be included in all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
