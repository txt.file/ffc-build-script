#!/bin/bash

SCRIPT_FOLDER="$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )"
if [ ! -e "${SCRIPT_FOLDER}/gluon-secret.key" ]; then
	echo "${SCRIPT_FOLDER}/gluon-secret.key missing"
	exit 1
fi

if [ ! $# -eq 2 ]; then
	echo "please provide which GLUON_BRANCH should be build and where the output should be put"
	echo "e.g. as \"${SCRIPT_FOLDER}/build.sh stable ~/firmware\""
	exit 1
fi

# gluon branch
# if experimental or nightly then include BROKEN devices
GLUON_BRANCH=$1
BROKEN=0
case $1 in
	experimental|nightly)
		BROKEN=1
		;;
	stable)
		BROKEN=0
		;;
	*)
		echo "Please provide stable, experimental or nightly as parameter."
		echo "Other options are currently unsupported"
		exit 1
		;;
esac

OUTPUT_PATH="$2"/"$GLUON_BRANCH"
if [ ! -e "$OUTPUT_PATH" ]; then
	if ! mkdir -p "$OUTPUT_PATH"; then
		echo "unable to create $OUTPUT_PATH"
		exit 1
	fi
fi
if [ -e "$OUTPUT_PATH" ] && [ ! -d "$OUTPUT_PATH" ]; then
	echo "$OUTPUT_PATH exists but is no folder. It must be a folder."
	exit 1
fi
if [ -d "$OUTPUT_PATH" ] && [ ! -w "$OUTPUT_PATH" ]; then
	echo "$OUTPUT_PATH needs to be a writable folder."
	exit 1
fi

# build date + gluon release info
GLUON_RELEASE=$(date '+%Y%m%d')"+"
if [ "HEAD" = "$(git rev-parse --abbrev-ref HEAD)" ]; then
        GLUON_RELEASE="$GLUON_RELEASE$(git describe --tags)"
else
        GLUON_RELEASE="$GLUON_RELEASE$(git rev-parse --abbrev-ref HEAD)-$(git rev-parse --short --verify HEAD)"
fi

# gluon targets
STABLE_TARGETS="$(make list-targets)"
ALL_TARGETS="$(make BROKEN=1 list-targets)"

# export everything
export GLUON_RELEASE
export STABLE_TARGETS
export ALL_TARGETS
export GLUON_BRANCH
export BROKEN
export OUTPUT_PATH
export SCRIPT_FOLDER
