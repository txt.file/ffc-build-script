#!/bin/bash

# This is a build script to run make for all the supported GLUON_TARGETS and our gluon site configurations.
# Copyright (C) 2017,2019,2020 Vieno Hakkerinen
# SPDX-License-Identifier: MIT

. "$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )/env.sh"

rm -r output/*
for TARGET in $ALL_TARGETS; do
	echo "### starting to build $TARGET ###"
	if ! make -j"$(nproc || echo -n 1)" GLUON_TARGET="$TARGET" BROKEN="$BROKEN"; then
		echo "$(date -Isecond) could not build $TARGET for " >> build.messages
		exit
	fi
	echo "### finished to build $TARGET ###"
done
make manifest GLUON_BRANCH="$GLUON_BRANCH" BROKEN="$BROKEN"
rm -r "$OUTPUT_PATH"/*
touch -m "$OUTPUT_PATH"/
cp -r output/images/* "$OUTPUT_PATH"/
./contrib/sign.sh "${SCRIPT_FOLDER}"/gluon-secret.key "$OUTPUT_PATH"/sysupgrade/"$GLUON_BRANCH".manifest
